#https://developer.hashicorp.com/terraform/tutorials/aws-get-started/aws-build

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
  backend "http" {}
  required_version = ">= 1.2.0"
}

provider "aws" {
  region  = "${var.aws-region}"
}

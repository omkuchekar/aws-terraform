variable "aws-region" {
    description = "AWS region variable"
    default = "us-east-2"
}

variable "cidr" {
  description = "VPC CIDR value"
  default = "10.0.0.0/16"
}

variable "ami-id" {
  description = "AMI image id"
  default = "ami-0578f2b35d0328762"
  
}

variable "instance-type" {
  default = "t2.micro"
}

variable "vpc-tag" {
    description = "vpc tag name"
    default = "terraformvpc"  
}

variable "instance-tenancy" {
  description = "VPC tenancy"
  default = "default"
}

variable "ec2-tag" {
    description = "ec2 tag name"
    default = "EC2terraform"
  
}
# below synatx can be get easily from terraform docs
#https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc


resource "aws_vpc" "myvpcterraform" {
  cidr_block       = "${var.cidr}"
  instance_tenancy = "${var.instance-tenancy}"

  tags = {
    Name = "${var.vpc-tag}"
  }
}

resource "aws_instance" "myEC2terraform" {
  ami           = "${var.ami-id}"
  instance_type = "${var.instance-type}"

  tags = {
    Name = "${var.ec2-tag}"
  }
}